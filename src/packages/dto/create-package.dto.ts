import { ApiHideProperty, ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber } from 'class-validator';

export class CreatePackageDto {
  @ApiProperty()
  @IsNotEmpty()
  title?: string;

  @ApiProperty()
  @IsNumber()
  discount?: number;

  @ApiProperty()
  @IsNumber()
  months?: number;

  @ApiHideProperty()
  _id?: string;
}
