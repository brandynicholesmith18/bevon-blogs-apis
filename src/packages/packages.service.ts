import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import ResponseHandler from '../common/ResponseHandler';
import { CreatePackageDto } from './dto/create-package.dto';

@Injectable()
export class PackagesService {
  constructor(
    @InjectModel('packages') private readonly packageModel,
    @InjectModel('basic-subscription-price') private readonly basicSubscriptionPriceModel
  ) {}

  async createPackage(packagee: CreatePackageDto) {
    let check = await this.packageModel
      .findOne({
        months: packagee.months
      })
      .lean();

    if (check) {
      return ResponseHandler.fail(
        `Could not create the package. Another package for ${packagee.months} month${packagee.months > 1 ? 's' : ''} already exists.`
      );
    }

    check = await this.packageModel
      .findOne({
        title: packagee.title
      })
      .lean();

    if (check) {
      return ResponseHandler.fail(`Could not create the package. Another package with this title already exists.`);
    }

    let newPackage = new this.packageModel(packagee);
    newPackage = await newPackage.save();

    return ResponseHandler.success(newPackage, 'Package created successfully.');
  }

  async updatePackage(id, packagee: CreatePackageDto) {
    let check = await this.packageModel
      .findOne({
        months: packagee.months
      })
      .lean();

    if (check && check._id.toString() !== id) {
      return ResponseHandler.fail(
        `Could not update the package. Another package for ${packagee.months} month${packagee.months > 1 ? 's' : ''} already exists.`
      );
    }

    check = await this.packageModel
      .findOne({
        title: packagee.title
      })
      .lean();

    if (check && check._id.toString() !== id) {
      return ResponseHandler.fail(`Could not update the package. Another package with this title already exists.`);
    }

    const newPackage = await this.packageModel
      .findByIdAndUpdate(
        id,
        {
          $set: packagee
        },
        { new: true }
      )
      .lean();

    if (newPackage) {
      return ResponseHandler.success(newPackage, 'Package details updated successfully.');
    } else {
      return ResponseHandler.fail('Package not found.');
    }
  }

  async getPackages() {
    const packages = await this.packageModel.find().lean();
    const { price } = await this.basicSubscriptionPriceModel.findOne().lean();

    return ResponseHandler.success({ packages, basePrice: price });
  }

  async getPackageById(id, lean = false) {
    let packagee = this.packageModel.findById(id);
    if (lean) {
      packagee = await packagee.lean();
    } else {
      packagee = await packagee.exec();
    }

    return ResponseHandler.success(packagee);
  }
}
