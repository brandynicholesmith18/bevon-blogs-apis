import { Body, Controller, HttpCode, Param, Post, UseGuards } from '@nestjs/common';
import { PackagesService } from './packages.service';
import { ApiBearerAuth, ApiConsumes, ApiOperation, ApiTags } from '@nestjs/swagger';
import { CreatePackageDto } from './dto/create-package.dto';
import { PackageIdDto } from '../common/dto';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from '../auth/roles.guard';
import { Roles } from '../auth/roles.decorator';

@ApiTags('Admin Portal - Packages')
@Controller('/admin/packages')
export class PackagesAdminController {
  constructor(private readonly packagesService: PackagesService) {}

  @ApiBearerAuth()
  @ApiOperation({ summary: 'Create new package.' })
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles('superAdmin', 'admin')
  @Post()
  @HttpCode(200)
  @ApiConsumes('application/x-www-form-urlencoded')
  async createPackage(@Body() createPackageDto: CreatePackageDto) {
    return await this.packagesService.createPackage(createPackageDto);
  }

  @ApiBearerAuth()
  @ApiOperation({ summary: 'Update an existing package.' })
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles('superAdmin', 'admin')
  @Post('/update/:id')
  @HttpCode(200)
  @ApiConsumes('application/x-www-form-urlencoded')
  async updatePackage(@Body() createPackageDto: CreatePackageDto, @Param() packageIdDto: PackageIdDto) {
    delete createPackageDto._id;
    return await this.packagesService.updatePackage(packageIdDto.id, createPackageDto);
  }
}
