import { Module } from '@nestjs/common';
import { PackagesController } from './packages.controller';
import { PackagesService } from './packages.service';
import { MongooseModule } from '@nestjs/mongoose';
import { PackageSchema } from './package.model';
import { PackagesAdminController } from './packages-admin.controller';
import { BasicSubscriptionPriceSchema } from 'src/subscriptions/basic-subscription.model';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'packages', schema: PackageSchema },
      { name: 'basic-subscription-price', schema: BasicSubscriptionPriceSchema }
    ])
  ],
  controllers: [PackagesController, PackagesAdminController],
  providers: [PackagesService],
  exports: [PackagesService]
})
export class PackagesModule {}
