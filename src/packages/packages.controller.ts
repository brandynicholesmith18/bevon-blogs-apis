import { Controller, Get } from '@nestjs/common';
import { PackagesService } from './packages.service';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('User Portal - Packages')
@Controller('packages')
export class PackagesController {
  constructor(private readonly packagesService: PackagesService) {}

  @Get()
  async getPackages() {
    return await this.packagesService.getPackages();
  }
}
