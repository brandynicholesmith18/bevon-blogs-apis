import * as mongoose from 'mongoose';

export const PackageSchema = new mongoose.Schema(
  {
    title: { type: String },
    months: { type: Number, default: 1 },
    discount: { type: Number, default: 0 }
  },
  {
    timestamps: true
  }
);
