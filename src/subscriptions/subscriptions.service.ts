import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { SubscriptionsListDto } from './dto/subscriptions-list.dto';
import { CreateSubscriptionDto } from './dto/create-subscription.dto';
import { PackagesService } from '../packages/packages.service';
import { UsersService } from '../users/users.service';
import { BlogCategoriesService } from '../blog-categories/blog-categories.service';
import ResponseHandler from '../common/ResponseHandler';
import * as mongoose from 'mongoose';
import * as moment from 'moment';
import uuidAPIKey from 'uuid-apikey';
import { SquareService } from 'src/square/square.service';

@Injectable()
export class SubscriptionsService {
  constructor(
    @InjectModel('subscriptions') private readonly subscriptionModel,
    @InjectModel('basic-subscription-price') private readonly basicSubscriptionPriceModel,
    private readonly usersService: UsersService,
    private readonly squareService: SquareService,
    private readonly packagesService: PackagesService,
    private readonly blogCategoriesService: BlogCategoriesService
  ) {}

  async getCurrentlyValidUserSubscription(userId, lean = true) {
    let subscription = this.subscriptionModel.findOne({
      user: userId,
      validTill: { $gte: new Date() },
      validFrom: { $lte: new Date() }
    });

    if (lean) {
      subscription = await subscription.lean();
    } else {
      subscription = await subscription.exec();
    }

    return ResponseHandler.success(subscription);
  }

  async createSubscription(subscriptionParams: CreateSubscriptionDto) {
    const { data: packagee } = await this.packagesService.getPackageById(subscriptionParams.packageId, true);

    if (packagee) {
      const { data: user } = await this.usersService.getUserById(subscriptionParams.userId, false, '+apiKey');
      const { price: basicPrice } = await this.basicSubscriptionPriceModel.findOne({}).lean();
      const { data: categories } = await this.blogCategoriesService.getBlogCategoriesByIds(subscriptionParams.additionalCategories);
      let additionalPrice = 0;
      categories.map((category) => {
        additionalPrice += category.price;
      });

      const totalPrice = (basicPrice + additionalPrice) * packagee.months;
      const discount = (packagee.discount * totalPrice) / 100;
      const discountedPrice = totalPrice - discount;
      const discountedPriceWithTrxCharges = (discountedPrice + 0.3) / 0.971;

      let transId = null;
      try {
        const { data: payment } = await this.squareService.createPayment({
          sourceId: subscriptionParams.sourceId,
          idempotencyKey: subscriptionParams.idempotencyKey,
          amount: Number((discountedPriceWithTrxCharges * 100).toFixed())
        });
        transId = payment.id;

        // transId = charge.transId;
        if (!user.apiKey) {
          const uuid = uuidAPIKey.create();
          user.apiKey = uuid.apiKey;
          user.apiKeyUUID = uuid.uuid;
          await user.save();
        }
      } catch (e) {
        return ResponseHandler.fail(e.response && e.response.message ? e.response.message : e);
      }

      try {
        let subValidFrom = null;
        const { data: latestSubscription } = await this.getLatestSubscription(subscriptionParams.userId);
        if (latestSubscription) {
          subValidFrom = new Date() > new Date(latestSubscription.validTill) ? new Date() : new Date(latestSubscription.validTill);
          if (user.subscriptionValidTill < latestSubscription.validFrom) {
            user.subscriptionValidFrom = subValidFrom;
          }
        } else {
          subValidFrom = new Date(moment().startOf('d').toISOString());
          user.subscriptionValidFrom = subValidFrom;
        }
        const subValidTill = new Date(moment(subValidFrom).add(packagee.months, 'month').toISOString());
        user.subscriptionValidTill = subValidTill;
        await user.save();

        const subscription = {
          user: subscriptionParams.userId,
          package: subscriptionParams.packageId,
          additionalCategories:
            subscriptionParams.additionalCategories && subscriptionParams.additionalCategories.length > 0
              ? subscriptionParams.additionalCategories
              : [],
          validFrom: subValidFrom,
          validTill: subValidTill,
          transactionId: transId,
          totalPrice,
          basicPrice,
          additionalPrice,
          discount,
          discountPercentage: packagee.discount,
          totalPaid: discountedPriceWithTrxCharges,
          totalRevenue: discountedPrice,
          authorizeNetFee: discountedPriceWithTrxCharges - discountedPrice
        };
        let newSubscription = await this.subscriptionModel(subscription);
        newSubscription = await newSubscription.save();
        return ResponseHandler.success({ subscription: newSubscription, apiKey: user.apiKey }, 'Subscribed for the package successfully.');
      } catch (e) {
        return ResponseHandler.fail(e.response ? e.response.data.message : e.message);
      }
    } else {
      return ResponseHandler.fail(`Package doesn't exist.`);
    }
  }

  async getLatestSubscription(userId) {
    const pipeline = [];
    pipeline.push(
      ...[
        {
          $match: {
            user: mongoose.Types.ObjectId(userId),
            validTill: { $gte: new Date() }
          }
        },
        {
          $sort: {
            validTill: -1
          }
        }
      ]
    );

    const subscriptions = await this.subscriptionModel.aggregate(pipeline).exec();

    return ResponseHandler.success(subscriptions.length > 0 ? subscriptions[0] : null);
  }

  async getSubscriptions(params: SubscriptionsListDto, userId = null) {
    const { keyword, perPage, page, sortOrder, sortBy } = params;

    const sort = {};
    sort[sortBy] = sortOrder;

    const pipeline = [];

    if (userId) {
      pipeline.push({
        $match: {
          user: mongoose.Types.ObjectId(userId)
        }
      });
    }

    pipeline.push(
      ...[
        {
          $lookup: {
            from: 'users',
            foreignField: '_id',
            localField: 'user',
            as: 'user'
          }
        },
        {
          $lookup: {
            from: 'packages',
            foreignField: '_id',
            localField: 'package',
            as: 'package'
          }
        },
        {
          $lookup: {
            from: 'blog-categories',
            foreignField: '_id',
            localField: 'additionalCategories',
            as: 'additionalCategories'
          }
        },
        {
          $addFields: {
            user: { $arrayElemAt: ['$user', 0] },
            package: { $arrayElemAt: ['$package', 0] }
          }
        },
        {
          $match: {
            $or: [
              {
                'user.fullName': {
                  $regex: new RegExp(keyword),
                  $options: 'i'
                }
              },
              {
                'package.title': {
                  $regex: new RegExp(keyword),
                  $options: 'i'
                }
              }
            ]
          }
        },
        {
          $sort: sort
        }
      ]
    );

    const subscriptions = await this.subscriptionModel
      .aggregate(pipeline)
      .collation({ locale: 'en', strength: 2 })
      // .skip((page - 1) * perPage)
      // .limit(perPage)
      .exec();
    const { data: rows } = await this.getSubscriptionRows(params, userId);

    return ResponseHandler.success({
      subscriptions,
      rows
    });
  }

  async getSubscriptionRows(params: SubscriptionsListDto, userId) {
    const { keyword } = params;

    const pipeline = [];

    if (userId) {
      pipeline.push({
        $match: {
          user: mongoose.Types.ObjectId(userId)
        }
      });
    }

    pipeline.push(
      ...[
        {
          $lookup: {
            from: 'users',
            foreignField: '_id',
            localField: 'user',
            as: 'user'
          }
        },
        {
          $lookup: {
            from: 'packages',
            foreignField: '_id',
            localField: 'package',
            as: 'package'
          }
        },
        {
          $addFields: {
            user: { $arrayElemAt: ['$user', 0] },
            package: { $arrayElemAt: ['$package', 0] }
          }
        },
        {
          $match: {
            $or: [
              {
                'user.fullName': {
                  $regex: new RegExp(keyword),
                  $options: 'i'
                }
              },
              {
                'package.title': {
                  $regex: new RegExp(keyword),
                  $options: 'i'
                }
              }
            ]
          }
        },
        {
          $group: {
            _id: null,
            rows: { $sum: 1 }
          }
        }
      ]
    );

    const rows = await this.subscriptionModel.aggregate(pipeline).exec();

    return ResponseHandler.success(rows.length > 0 ? rows[0].rows : 0);
  }

  async updateBasicSubscriptionPrice(price) {
    const { price: newPrice } = await this.basicSubscriptionPriceModel.findOneAndUpdate(
      {},
      { $set: { price } },
      { new: true, upsert: true }
    );

    return ResponseHandler.success(newPrice, 'Basic subscription price updated successfully.');
  }
}
