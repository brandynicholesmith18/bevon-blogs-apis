import { forwardRef, Module } from '@nestjs/common';
import { SubscriptionsController } from './subscriptions.controller';
import { SubscriptionsService } from './subscriptions.service';
import { MongooseModule } from '@nestjs/mongoose';
import { SubscriptionSchema } from './subscription.model';
import { PackagesModule } from '../packages/packages.module';
import { UsersModule } from '../users/users.module';
import { SubscriptionsAdminController } from './subscriptions-admin.controller';
import { BasicSubscriptionPriceSchema } from './basic-subscription.model';
import { BlogCategoriesModule } from '../blog-categories/blog-categories.module';
import { SquareModule } from 'src/square/square.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'subscriptions', schema: SubscriptionSchema },
      { name: 'basic-subscription-price', schema: BasicSubscriptionPriceSchema }
    ]),
    forwardRef(() => UsersModule),
    forwardRef(() => PackagesModule),
    forwardRef(() => SquareModule),
    forwardRef(() => BlogCategoriesModule)
  ],
  controllers: [SubscriptionsController, SubscriptionsAdminController],
  providers: [SubscriptionsService],
  exports: [SubscriptionsService]
})
export class SubscriptionsModule {}
