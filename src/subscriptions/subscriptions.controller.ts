import { Body, Controller, Get, HttpCode, Post, Query, UseGuards } from '@nestjs/common';
import { SubscriptionsService } from './subscriptions.service';
import { SubscriptionsListDto } from './dto/subscriptions-list.dto';
import { GetUser } from '../auth/get-user.decorator';
import { ApiBearerAuth, ApiConsumes, ApiOperation, ApiSecurity, ApiTags } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from '../auth/roles.guard';
import { Roles } from '../auth/roles.decorator';
import { CreateSubscriptionDto } from './dto/create-subscription.dto';
import { PriceDto } from '../common/dto/price.dto';

@ApiTags('User Portal - Subscriptions')
@Controller('subscriptions')
export class SubscriptionsController {
  constructor(private readonly subscriptionsService: SubscriptionsService) {}

  @ApiSecurity('API-KEY', ['API-KEY'])
  @UseGuards(AuthGuard('headerapikey'))
  @ApiOperation({ summary: 'Create a user subscription.' })
  @Post()
  @HttpCode(200)
  @ApiConsumes('application/x-www-form-urlencoded')
  async createSubscription(@Body() createSubscriptionDto: CreateSubscriptionDto, @GetUser() user) {
    createSubscriptionDto.userId = user._id;

    return await this.subscriptionsService.createSubscription(createSubscriptionDto);
  }

  @ApiSecurity('API-KEY', ['API-KEY'])
  @UseGuards(AuthGuard('headerapikey'))
  @ApiOperation({ summary: `Get user's subscriptions.` })
  @Get()
  async getSubscriptions(@Query() subscriptionsListDto: SubscriptionsListDto, @GetUser() user) {
    subscriptionsListDto.keyword = subscriptionsListDto.keyword ? subscriptionsListDto.keyword : '';

    return await this.subscriptionsService.getSubscriptions(subscriptionsListDto, user._id);
  }
}
