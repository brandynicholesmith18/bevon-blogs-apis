import { ApiHideProperty, ApiProperty } from '@nestjs/swagger';
import { IsArray, IsMongoId, IsNotEmpty, IsOptional, ValidateNested } from 'class-validator';
import { CardDto } from '../../common/dto';
import { Type } from 'class-transformer';
import responseMessages from '../../config/responseMessages';

export class CreateSubscriptionDto {
  @ApiProperty()
  @IsMongoId({ message: responseMessages.common.invalidPackageId })
  packageId?: string;

  @ApiProperty()
  @IsOptional()
  @IsArray({ message: responseMessages.createSubscription.additionalCategories })
  @IsMongoId({ message: responseMessages.common.invalidBlogCategoryId, each: true })
  additionalCategories?: string[];

  @IsNotEmpty({ message: responseMessages.createSubscription.idempotencyKey })
  idempotencyKey?: string;

  @IsNotEmpty({ message: responseMessages.createSubscription.sourceId })
  sourceId?: string;

  @ApiHideProperty()
  userId?: string;
}
