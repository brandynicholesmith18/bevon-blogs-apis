import * as mongoose from 'mongoose';

export const BasicSubscriptionPriceSchema = new mongoose.Schema(
  {
    price: { type: Number, default: 0 }
  },
  {
    timestamps: true
  }
);
