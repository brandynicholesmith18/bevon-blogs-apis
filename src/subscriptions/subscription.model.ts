import * as mongoose from 'mongoose';

export const SubscriptionSchema = new mongoose.Schema(
  {
    user: { type: mongoose.SchemaTypes.ObjectId, ref: 'users' },
    package: { type: mongoose.SchemaTypes.ObjectId, ref: 'packages' },
    validTill: { type: Date },
    validFrom: { type: Date },
    transactionId: { type: String, default: null },
    basicPrice: { type: Number, default: 0 },
    additionalPrice: { type: Number, default: 0 },
    totalPrice: { type: Number, default: 0 },
    discountPercentage: { type: Number, default: 0 },
    discount: { type: Number, default: 0 },
    totalPaid: { type: Number, default: 0 },
    totalRevenue: { type: Number, default: 0 },
    authorizeNetFee: { type: Number, default: 0 },
    additionalCategories: [{ type: mongoose.SchemaTypes.ObjectId, ref: 'blog-categories' }]
  },
  {
    timestamps: true
  }
);
