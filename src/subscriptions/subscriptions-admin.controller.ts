import { Body, Controller, Get, HttpCode, Post, Query, UseGuards } from '@nestjs/common';
import { SubscriptionsService } from './subscriptions.service';
import { SubscriptionsListDto } from './dto/subscriptions-list.dto';
import { ApiBearerAuth, ApiConsumes, ApiOperation, ApiTags } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from '../auth/roles.guard';
import { Roles } from '../auth/roles.decorator';
import { PriceDto } from '../common/dto/price.dto';

@ApiTags('Admin Portal - Subscriptions')
@Controller('/admin/subscriptions')
export class SubscriptionsAdminController {
  constructor(private readonly subscriptionsService: SubscriptionsService) {}

  @Get()
  async getSubscriptions(@Query() subscriptionsListDto: SubscriptionsListDto) {
    subscriptionsListDto.keyword = subscriptionsListDto.keyword ? subscriptionsListDto.keyword : '';

    return await this.subscriptionsService.getSubscriptions(subscriptionsListDto);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles('superAdmin')
  @ApiOperation({ summary: 'Update basic subscription price.' })
  @Post('update-basic-price')
  @HttpCode(200)
  @ApiConsumes('application/x-www-form-urlencoded')
  async updateBasicSubscriptionPrice(@Body() priceDto: PriceDto) {
    return await this.subscriptionsService.updateBasicSubscriptionPrice(priceDto.price);
  }
}
