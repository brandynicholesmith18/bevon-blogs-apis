import { NestFactory } from '@nestjs/core';
import { checkAuth } from './middlewares/checkAuthorization.middleware';
import { AppModule } from './app.module';
import { PORT, HOSTED_ON } from './config/config';
import { urlencoded, json } from 'express';
import { join } from 'path';
import * as fs from 'fs';
import { ValidationPipe } from './common';
import { NestExpressApplication } from '@nestjs/platform-express';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import * as kill from 'kill-port';

(async () => {
  async function bootstrap() {
    const app = await NestFactory.create<NestExpressApplication>(AppModule);

    app.useGlobalPipes(new ValidationPipe());
    app.use(checkAuth);

    app.enableCors();
    app.use((req, res, next) => {
      res.header('Access-Control-Allow-Origin', '*');
      next();
    });

    app.use(json({ limit: '50mb' }));
    app.use(urlencoded({ extended: true, limit: '50mb' }));

    app.useStaticAssets(join(__dirname, '..', 'public'));

    app.setGlobalPrefix('api');

    const swaggerOptions = new DocumentBuilder()
      .addBearerAuth()
      .addApiKey({ type: 'apiKey', name: 'API-KEY', in: 'header' }, 'API-KEY')
      .setTitle('Blog APIs Documentation')
      .setDescription('Blog documentation page for describing college, admin and user portal APIs')
      .setVersion('1.0')
      .build();
    const document = SwaggerModule.createDocument(app, swaggerOptions);
    fs.writeFileSync('./public/swagger-spec.json', JSON.stringify(document));
    SwaggerModule.setup(`/api/documentation`, app, document);

    kill(Number(PORT), 'tcp').then(async () => await app.listen(Number(PORT)));
  }
  await bootstrap();
})();
