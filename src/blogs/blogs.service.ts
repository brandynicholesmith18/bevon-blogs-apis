import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { SubscriptionsService } from '../subscriptions/subscriptions.service';
import { BlogsUsersListDto } from './dto/blogs-users-list.dto';
import { BlogCategoriesService } from '../blog-categories/blog-categories.service';
import { BlogDetailsMatchInterface, BlogsListMatchInterface } from './interfaces';
import { BlogsAdminsListDto } from './dto/blogs-admins-list.dto';

import ResponseHandler from '../common/ResponseHandler';
import { CreateBlogDto } from './dto/create-blog.dto';
import { NumericIdsService } from '../numeric-ids/numeric-ids.service';
import { Types } from 'mongoose';

@Injectable()
export class BlogsService {
  constructor(
    @InjectModel('blogs') private readonly blogModel,
    private readonly subscriptionsService: SubscriptionsService,
    private readonly blogCategoriesService: BlogCategoriesService,
    private readonly numericIdsService: NumericIdsService
  ) {}

  async createBlog(blog: CreateBlogDto) {
    const { data: id } = await this.numericIdsService.getNextBlogId();
    blog.numId = id;
    const newBlog = await this.blogModel.create(blog);

    return ResponseHandler.success(newBlog, 'Blog created successfully.');
  }

  async editBlog(id: string, blog: CreateBlogDto) {
    const newBlog = await this.blogModel
      .findByIdAndUpdate(
        id,
        {
          $set: blog
        },
        { new: true }
      )
      .lean();

    if (newBlog) {
      return ResponseHandler.success(newBlog, 'Blog updated successfully.');
    } else {
      return ResponseHandler.fail('Blog not found');
    }
  }

  async setBlogStatus({ id, status }) {
    const newBlog = await this.blogModel
      .findByIdAndUpdate(
        id,
        {
          $set: { status }
        },
        { new: true }
      )
      .lean();

    if (newBlog) {
      return ResponseHandler.success(newBlog, `Blog status set to ${status} successfully.`);
    } else {
      return ResponseHandler.fail('Blog not found.');
    }
  }

  async getBlogsForUsers(params: BlogsUsersListDto, userId) {
    const { data: subscription } = await this.subscriptionsService.getCurrentlyValidUserSubscription(userId);
    if (subscription) {
      const { data: categories } = await this.blogCategoriesService.getBlogCategoriesForUser(subscription.additionalCategories);
      const validCatIds = categories.map((category) => {
        return category._id;
      });

      const { keyword, page, perPage, sortOrder, sortBy, categoryId } = params;

      const sort = {};
      sort[sortBy] = Number(sortOrder);

      const find: BlogsListMatchInterface = {
        category: categoryId && validCatIds.find((id) => categoryId.toString() === id.toString()) ? categoryId : categories,
        title: { $regex: new RegExp(keyword), $options: 'i' },
        status: 'published'
      };

      const blogs = await this.blogModel
        .find(find)
        .collation({ locale: 'en', strength: 2 })
        .sort(sort)
        .skip((page - 1) * perPage)
        .limit(perPage)
        .lean();

      return ResponseHandler.success(blogs);
    } else {
      return ResponseHandler.fail('Please contact your administrator for access to blogs.');
    }
  }

  async getBlogDetailsForUsers(numId, userId) {
    const { data: subscription } = await this.subscriptionsService.getCurrentlyValidUserSubscription(userId);
    if (subscription) {
      const { data: categories } = await this.blogCategoriesService.getBlogCategoriesForUser(subscription.additionalCategories);

      const find: BlogDetailsMatchInterface = {
        category: categories,
        numId
      };

      const blog = await this.blogModel.findOne({ numId }).lean();

      return ResponseHandler.success(blog);
    } else {
      return ResponseHandler.fail('Please contact your administrator for access to this blog.');
    }
  }

  async getBlogDetailsForAdmin(id) {
    const blog = await this.blogModel.findById(id).populate('category').lean();

    return ResponseHandler.success(blog);
  }

  async getBlogsForAdmins(params: BlogsAdminsListDto) {
    const { keyword, category, page, perPage, sortBy, sortOrder } = params;

    const sort = [];
    sort[sortBy] = sortOrder;

    const find: BlogsListMatchInterface = {
      title: { $regex: new RegExp(keyword), $options: 'i' }
    };

    if (category) {
      find.category = category;
    }

    const blogs = await this.blogModel.find(find).populate('category').sort(sort).paginate(page, perPage).lean();
    const rows = await this.blogModel.countDocuments(find).exec();

    return ResponseHandler.success({ blogs, rows });
  }

  async deleteBlog(id) {
    await this.blogModel.deleteOne({ _id: Types.ObjectId(id) }).exec();

    return ResponseHandler.success();
  }
}
