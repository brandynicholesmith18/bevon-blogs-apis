import { Body, Controller, Delete, Get, HttpCode, Param, Post, Query, UploadedFiles, UseGuards, UseInterceptors } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { GetUser } from '../auth/get-user.decorator';
import { ApiBearerAuth, ApiConsumes, ApiOperation, ApiTags } from '@nestjs/swagger';
import { BlogsService } from './blogs.service';
import { BlogsAdminsListDto } from './dto/blogs-admins-list.dto';
import { CreateBlogDto } from './dto/create-blog.dto';
import { FileFieldsInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import * as fs from 'fs';
import ResponseHandler from '../common/ResponseHandler';
import { BlogIdDto } from '../common/dto/blog-id.dto';
import { SetBlogStatusDto } from './dto/set-blog-status.dto';
import { MongoIdDto } from 'src/common/dto/mongo-id.dto';

@ApiTags('Admin Portal - Blogs')
@Controller('admin-blogs')
export class BlogsAdminController {
  constructor(private readonly blogsService: BlogsService) {}

  @ApiBearerAuth()
  @ApiOperation({ summary: 'Get paginated blogs list for admin panel.' })
  @UseGuards(AuthGuard('jwt'))
  @Get()
  async getBlogs(@Query() blogsAdminsListDto: BlogsAdminsListDto, @GetUser() user) {
    return await this.blogsService.getBlogsForAdmins(blogsAdminsListDto);
  }

  @ApiBearerAuth()
  @ApiOperation({ summary: 'Get blog details.' })
  @UseGuards(AuthGuard('jwt'))
  @Get('/details/:id')
  async getBlogDetails(@Param() blogIdDto: BlogIdDto) {
    return await this.blogsService.getBlogDetailsForAdmin(blogIdDto.id);
  }

  @ApiBearerAuth()
  @ApiOperation({ summary: 'Set blog status.' })
  @UseGuards(AuthGuard('jwt'))
  @Post('/set-status/:id')
  @HttpCode(200)
  @ApiConsumes('application/x-www-form-urlencoded')
  async setBlogStatus(@Body() setBlogStatusDto: SetBlogStatusDto, @Param() blogIdDto: BlogIdDto) {
    return await this.blogsService.setBlogStatus({ ...setBlogStatusDto, ...blogIdDto });
  }

  @ApiBearerAuth()
  @ApiOperation({ summary: 'Create new blog' })
  @UseGuards(AuthGuard('jwt'))
  @Post()
  @UseInterceptors(
    FileFieldsInterceptor([{ name: 'headerImage', maxCount: 1 }], {
      storage: diskStorage({
        destination: (req, file, cb) => {
          fs.mkdir('./public/uploads/blogs-images', { recursive: true }, (err) => {
            if (err) {
              return ResponseHandler.fail(err.message);
            }
            cb(null, './public/uploads/blogs-images');
          });
        },
        filename: (req, file, cb) => {
          let name = file.originalname.split('.');
          cb(null, name[0] + '-' + Date.now() + '.' + name[name.length - 1]);
        }
        // onError: err => {},
      })
    })
  )
  @HttpCode(200)
  @ApiConsumes('multipart/form-data')
  async createBlog(@Body() createBlogDto: CreateBlogDto, @UploadedFiles() files) {
    createBlogDto.headerImage = files && files.headerImage ? '/uploads/blogs-images/' + files.headerImage[0].filename : '';

    return await this.blogsService.createBlog(createBlogDto);
  }

  @ApiBearerAuth()
  @ApiOperation({ summary: 'Edit a blog' })
  @UseGuards(AuthGuard('jwt'))
  @Post('/edit/:id')
  @UseInterceptors(
    FileFieldsInterceptor([{ name: 'headerImage', maxCount: 1 }], {
      storage: diskStorage({
        destination: (req, file, cb) => {
          fs.mkdir('./public/uploads/blogs-images', { recursive: true }, (err) => {
            if (err) {
              return ResponseHandler.fail(err.message);
            }
            cb(null, './public/uploads/blogs-images');
          });
        },
        filename: (req, file, cb) => {
          let name = file.originalname.split('.');
          cb(null, name[0] + '-' + Date.now() + '.' + name[name.length - 1]);
        }
        // onError: err => {},
      })
    })
  )
  @HttpCode(200)
  @ApiConsumes('multipart/form-data')
  async editBlog(@Param() blogIdDto: BlogIdDto, @Body() createBlogDto: CreateBlogDto, @UploadedFiles() files) {
    if (files && files.headerImage) {
      createBlogDto.headerImage = '/uploads/blogs-images/' + files.headerImage[0].filename;
    } else if (createBlogDto.removeHeaderImage) {
      createBlogDto.headerImage = '';
    } else {
      delete createBlogDto.headerImage;
    }
    delete createBlogDto._id;

    return await this.blogsService.editBlog(blogIdDto.id, createBlogDto);
  }

  @Delete(':id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Delete a blog.' })
  @UseGuards(AuthGuard('jwt'))
  async deleteBlog(@Param() mongoIdDto: MongoIdDto) {
    return await this.blogsService.deleteBlog(mongoIdDto.id);
  }
}
