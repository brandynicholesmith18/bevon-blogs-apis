import { IsIn } from 'class-validator';

export class SetBlogStatusDto {
  @IsIn(['draft', 'published', 'unpublished'])
  status: string;
}
