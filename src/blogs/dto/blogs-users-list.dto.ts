import { ListDto } from '../../common/dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsMongoId, IsOptional } from 'class-validator';
import responseMessages from '../../config/responseMessages';

export class BlogsUsersListDto extends ListDto {
  @ApiProperty({ required: false })
  @IsOptional()
  @IsMongoId({ message: responseMessages.common.invalidBlogCategoryId })
  categoryId?: string;
}
