import { ListDto } from '../../common/dto';

export class BlogsAdminsListDto extends ListDto {
  category?: string;
}
