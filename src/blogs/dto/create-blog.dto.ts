import { IsDateString, IsIn, IsMongoId, IsNotEmpty } from 'class-validator';
import { ApiHideProperty, ApiProperty } from '@nestjs/swagger';

export class CreateBlogDto {
  @ApiProperty({ type: 'string', format: 'binary' })
  headerImage?: string;

  @ApiProperty()
  removeHeaderImage?: boolean;

  @ApiProperty()
  altText?: string;

  @ApiProperty()
  permalink?: string;

  @ApiProperty()
  @IsNotEmpty()
  content: string;

  @ApiProperty()
  featured: boolean;

  @ApiProperty()
  @IsNotEmpty()
  title: string;

  @ApiProperty()
  @IsNotEmpty()
  tagLine: string;

  @ApiProperty()
  excerpt: string;

  @ApiProperty()
  @IsDateString()
  publishDate: string;

  @ApiProperty()
  @IsIn(['draft', 'published', 'unpublished'])
  status: string;

  @ApiProperty()
  @IsMongoId()
  category: string;

  @ApiHideProperty()
  numId?: number;

  @ApiHideProperty()
  _id?: string;
}
