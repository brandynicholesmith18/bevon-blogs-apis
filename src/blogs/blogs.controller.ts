import { Controller, Get, Param, Query, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { GetUser } from '../auth/get-user.decorator';
import { ApiOperation, ApiSecurity, ApiTags } from '@nestjs/swagger';
import { BlogsService } from './blogs.service';
import { BlogsUsersListDto } from './dto/blogs-users-list.dto';
import { NumIdDto } from 'src/common/dto/num-id.dto';

@ApiTags('Users Portal - Blogs')
@Controller('blogs')
export class BlogsController {
  constructor(private readonly blogsService: BlogsService) {}

  @ApiOperation({ summary: 'Get blogs list.' })
  @ApiSecurity('API-KEY', ['API-KEY'])
  @UseGuards(AuthGuard('headerapikey'))
  @Get()
  async getBlogs(@Query() blogsUsersListDto: BlogsUsersListDto, @GetUser() user) {
    return await this.blogsService.getBlogsForUsers(blogsUsersListDto, user._id);
  }

  @ApiOperation({ summary: 'Get blog details.' })
  @ApiSecurity('API-KEY', ['API-KEY'])
  @UseGuards(AuthGuard('headerapikey'))
  @Get(':id')
  async getBlogDetailss(@Param() numIdDto: NumIdDto, @GetUser() user) {
    return await this.blogsService.getBlogDetailsForUsers(numIdDto.id, user._id);
  }
}
