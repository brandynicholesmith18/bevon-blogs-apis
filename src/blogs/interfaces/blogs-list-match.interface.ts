interface KeywordSearchInterface {
  $regex: RegExp;
  $options: string;
}

export interface BlogsListMatchInterface {
  category?: string;
  title?: string | KeywordSearchInterface;
  status?: string;
}

export interface BlogDetailsMatchInterface {
  numId: number;
  category?: string;
}
