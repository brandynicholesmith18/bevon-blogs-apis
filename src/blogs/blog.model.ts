import * as mongoose from 'mongoose';

export const BlogSchema = new mongoose.Schema(
  {
    headerImage: { type: String },
    altText: { type: String },
    permalink: { type: String },
    content: { type: String, required: true },
    featured: { type: Boolean, default: false },
    title: { type: String, required: true },
    tagLine: { type: String, required: true },
    excerpt: { type: String, required: false },
    category: { type: mongoose.SchemaTypes.ObjectId, ref: 'blog-categories' },
    status: { type: String, enum: ['pending', 'draft', 'published', 'unpublished', 'denied'] },
    views: { type: Number, default: 0 },
    numId: { type: Number, unique: true },
    publishDate: { type: Date }
  },
  {
    timestamps: true
  }
);

BlogSchema.query.paginate = function (page, perPage) {
  return this.skip((page - 1) * perPage).limit(perPage);
};
