import { forwardRef, Module } from '@nestjs/common';
import { BlogsController } from './blogs.controller';
import { BlogsService } from './blogs.service';
import { MongooseModule } from '@nestjs/mongoose';
import { BlogSchema } from './blog.model';
import { UsersModule } from '../users/users.module';
import { SubscriptionsModule } from '../subscriptions/subscriptions.module';
import { BlogCategoriesModule } from '../blog-categories/blog-categories.module';
import { BlogsAdminController } from './blogs-admin.controller';
import { NumericIdsModule } from '../numeric-ids/numeric-ids.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'blogs', schema: BlogSchema }]),
    forwardRef(() => UsersModule),
    forwardRef(() => SubscriptionsModule),
    forwardRef(() => BlogCategoriesModule),
    forwardRef(() => NumericIdsModule)
  ],
  controllers: [BlogsController, BlogsAdminController],
  providers: [BlogsService],
  exports: [BlogsService]
})
export class BlogsModule {}
