import { Injectable } from '@nestjs/common';
import authorizeNet = require('authorizenet');
import { AUTHORIZE_NET_API_LOGIN_ID, AUTHORIZE_NET_TRANSACTION_KEY } from '../config/config';
import { CreateUserDto } from '../users/dto/create-user.dto';
import ResponseHandler, { SuccessInterface } from '../common/ResponseHandler';
import { CardDto } from '../common/dto';

const ApiContracts = authorizeNet.APIContracts;
const ApiControllers = authorizeNet.APIControllers;

@Injectable()
export class AuthorizeNetService {
  async createCharge(card: CardDto, amount: number) {
    const merchantAuthenticationType = new ApiContracts.MerchantAuthenticationType();
    merchantAuthenticationType.setName(AUTHORIZE_NET_API_LOGIN_ID);
    merchantAuthenticationType.setTransactionKey(AUTHORIZE_NET_TRANSACTION_KEY);

    const creditCard = new ApiContracts.CreditCardType();
    creditCard.setCardNumber(card.number);
    creditCard.setExpirationDate(card.expiryDate);
    creditCard.setCardCode(card.code);

    const paymentType = new ApiContracts.PaymentType();
    paymentType.setCreditCard(creditCard);

    const transactionRequestType = new ApiContracts.TransactionRequestType();
    transactionRequestType.setTransactionType(ApiContracts.TransactionTypeEnum.AUTHCAPTURETRANSACTION);
    transactionRequestType.setPayment(paymentType);
    transactionRequestType.setAmount(amount);

    const createRequest = new ApiContracts.CreateTransactionRequest();
    createRequest.setMerchantAuthentication(merchantAuthenticationType);
    createRequest.setTransactionRequest(transactionRequestType);

    try {
      const ctrl = new ApiControllers.CreateTransactionController(createRequest.getJSON());

      const { data, message } = await this.executeCreateChargeControl(ctrl);

      return ResponseHandler.success(data, message);
    } catch (e) {
      return ResponseHandler.fail(e.message ? e.message : e);
    }
  }

  async executeCreateChargeControl(ctrl) {
    return new Promise<SuccessInterface>((resolve, reject) => {
      ctrl.execute(() => {
        const apiResponse = ctrl.getResponse();

        const response = new ApiContracts.CreateTransactionResponse(apiResponse);

        if (response != null) {
          if (response.getMessages().getResultCode() === ApiContracts.MessageTypeEnum.OK) {
            if (response.getTransactionResponse().getMessages() != null) {
              return resolve(
                ResponseHandler.success(
                  response.transactionResponse,
                  response.getTransactionResponse().getMessages().getMessage()[0].getDescription()
                )
              );
            } else {
              return reject(response.getTransactionResponse().getErrors().getError()[0].getErrorText());
            }
          } else {
            if (response.getTransactionResponse() && response.getTransactionResponse().getErrors() != null) {
              return reject(response.getTransactionResponse().getErrors().getError()[0].getErrorText());
            } else {
              return reject(response.getMessages().getMessage()[0].getCode());
            }
          }
        } else {
          return reject('Null Response.');
        }
      });
    });
  }

  async createCustomer(customer: CreateUserDto) {
    const merchantAuthenticationType = new ApiContracts.MerchantAuthenticationType();
    merchantAuthenticationType.setName(AUTHORIZE_NET_API_LOGIN_ID);
    merchantAuthenticationType.setTransactionKey(AUTHORIZE_NET_TRANSACTION_KEY);

    const customerAddress = new ApiContracts.CustomerAddressType();
    customerAddress.setFirstName(customer.firstName);
    customerAddress.setLastName(customer.lastName);
    // customerAddress.setAddress(customer.address);
    // customerAddress.setCity(customer.city);
    // customerAddress.setState(customer.state ? (customer.state.shortName ? customer.state.shortName : customer.state) : '');
    // customerAddress.setZip(customer.zip);
    // customerAddress.setCountry(customer.country);
    // customerAddress.setPhoneNumber(customer.phone);

    const customerProfileType = new ApiContracts.CustomerProfileType();
    customerProfileType.setEmail(customer.email);

    const createRequest = new ApiContracts.CreateCustomerProfileRequest();
    createRequest.setProfile(customerProfileType);
    createRequest.setMerchantAuthentication(merchantAuthenticationType);

    const ctrl = new ApiControllers.CreateCustomerProfileController(createRequest.getJSON());

    try {
      const { data, message } = await this.executeCreateCustomerControl(ctrl);

      return ResponseHandler.success(data, message);
    } catch (e) {
      return ResponseHandler.fail(e.message);
    }
  }

  async executeCreateCustomerControl(ctrl) {
    return new Promise<SuccessInterface>((resolve, reject) => {
      ctrl.execute(() => {
        const apiResponse = ctrl.getResponse();

        const response = new ApiContracts.CreateCustomerProfileResponse(apiResponse);

        if (response != null) {
          if (response.getMessages().getResultCode() === ApiContracts.MessageTypeEnum.OK) {
            return resolve(
              ResponseHandler.success(response, 'Successfully created a customer profile with id: ' + response.getCustomerProfileId())
            );
          } else {
            return reject({
              message: response.getMessages().getMessage()[0].getText(),
              code: response.getMessages().getMessage()[0].getCode()
            });
          }
        } else {
          return reject('Null response received');
        }
      });
    });
  }

  async getCustomerPaymentProfileList(authorizeNetId) {
    const merchantAuthenticationType = new ApiContracts.MerchantAuthenticationType();
    merchantAuthenticationType.setName(AUTHORIZE_NET_API_LOGIN_ID);
    merchantAuthenticationType.setTransactionKey(AUTHORIZE_NET_TRANSACTION_KEY);

    const getRequest = new ApiContracts.GetCustomerProfileRequest();
    getRequest.setCustomerProfileId(authorizeNetId);
    getRequest.setMerchantAuthentication(merchantAuthenticationType);

    const ctrl = new ApiControllers.GetCustomerProfileController(getRequest.getJSON());

    try {
      const { data, message } = await this.executeGetCustomerPaymentProfileListControl(ctrl);

      return ResponseHandler.success(data, message);
    } catch (e) {
      return ResponseHandler.fail(e.message);
    }
  }

  async executeGetCustomerPaymentProfileListControl(ctrl) {
    return new Promise<SuccessInterface>((resolve, reject) => {
      ctrl.execute(() => {
        const apiResponse = ctrl.getResponse();

        const response = new ApiContracts.GetCustomerProfileResponse(apiResponse);

        if (response != null) {
          if (response.getMessages().getResultCode() === ApiContracts.MessageTypeEnum.OK) {
            const profiles = response.getProfile();
            // const paymentProfiles = profiles.map((profile) => profile.payment);
            return resolve(ResponseHandler.success(profiles));
            // for (let i = 0; i < profiles.length; i++) {
            //   console.log('Profile ID: ' + profiles[i].getCustomerProfileId());
            //   console.log('Payment Profile ID: ' + profiles[i].getCustomerPaymentProfileId());
            //   if (profiles[i].payment.creditCard) {
            //     console.log('Card: ' + profiles[i].payment.creditCard.cardNumber);
            //   } else if (profiles[i].payment.bankAccount) {
            //     console.log('Bank Account: ' + profiles[i].payment.bankAccount.accountNumber);
            //   }
            //   console.log('');
            // }
          } else {
            return reject({
              message: response.getMessages().getMessage()[0].getText(),
              code: response.getMessages().getMessage()[0].getCode()
            });
          }
        } else {
          return reject('Null response received');
        }
      });
    });
  }
}
