import { Injectable } from '@nestjs/common';
import { Client, CreatePaymentRequest, Environment } from 'square';
import ResponseHandler from 'src/common/ResponseHandler';

const client = new Client({
  environment: process.env.SQUARE_MODE === 'sandbox' ? Environment.Sandbox : Environment.Production,
  accessToken: process.env.SQUARE_ACCESS_TOKEN
});

@Injectable()
export class SquareService {
  async createPayment({ sourceId, idempotencyKey, amount }): Promise<any> {
    const body: CreatePaymentRequest = {
      sourceId,
      idempotencyKey,
      amountMoney: {
        amount,
        currency: 'USD'
      }
    };

    try {
      const {
        result: { payment }
      } = await client.paymentsApi.createPayment(body);
      return ResponseHandler.success(payment);
    } catch (error) {
      return ResponseHandler.fail(error.errors && error.errors.length > 0 ? error.errors[0].code : error);
    }
  }
}
