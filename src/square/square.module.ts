import { Module } from '@nestjs/common';
import { SquareService } from './square.service';

@Module({
  imports: [],
  providers: [SquareService],
  exports: [SquareService]
})
export class SquareModule {}
