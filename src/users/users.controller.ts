import { Body, Controller, Get, HttpCode, Post, UseGuards } from '@nestjs/common';
import { ApiConsumes, ApiOperation, ApiSecurity, ApiTags } from '@nestjs/swagger';
import { CreateUserDto } from './dto/create-user.dto';
import { UsersService } from './users.service';
import { AuthorizeNetService } from '../authorize-net/authorize-net.service';
import ResponseHandler from '../common/ResponseHandler';
import { AuthGuard } from '@nestjs/passport';
import { GetUser } from 'src/auth/get-user.decorator';

@ApiTags('User Portal - Users')
@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService, private readonly authorizeNetService: AuthorizeNetService) {}

  @ApiOperation({ summary: 'Create new user (Sign up).' })
  @Post()
  @HttpCode(200)
  @ApiConsumes('application/x-www-form-urlencoded')
  async createUser(@Body() createUserDto: CreateUserDto) {
    const { data: newCustomer, message } = await this.usersService.createUser(createUserDto);
    const { data: cust } = await this.authorizeNetService.createCustomer(newCustomer);
    newCustomer.authorizeNetId = cust.getCustomerProfileId();
    await newCustomer.save();

    return ResponseHandler.success(newCustomer, message);
  }

  @ApiOperation({ summary: 'Get user cards.' })
  @Get('cards')
  @ApiSecurity('API-KEY', ['API-KEY'])
  @UseGuards(AuthGuard('headerapikey'))
  async getCustomerCards(@GetUser() user) {
    return await this.authorizeNetService.getCustomerPaymentProfileList(user.authorizeNetId);
  }
}
