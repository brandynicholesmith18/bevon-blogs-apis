import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { AuthCredentialsDto } from '../auth/dto/authCredentialsDto';
import { CreateUserDto } from './dto/create-user.dto';
import { compareHash, getHash } from '../common/functions';
import { UsersListDto } from './dto/users-list.dto';
import * as mongoose from 'mongoose';

import ResponseHandler from '../common/ResponseHandler';

// @ts-ignore
// tslint:disable-next-line: no-var-requires
const uuidAPIKey = require('uuid-apikey');

@Injectable()
export class UsersService {
  constructor(@InjectModel('users') private readonly userModel) {}

  async createUser(user: CreateUserDto) {
    const check = await this.userModel.findOne({ email: user.email }).lean();

    if (!check) {
      const uuid = uuidAPIKey.create();
      user.apiKey = uuid.apiKey;
      user.apiKeyUUID = uuid.uuid;
      user.password = await getHash(user.password);
      user.fullName = `${user.firstName} ${user.lastName}`;

      let newUser = new this.userModel(user);
      newUser = await newUser.save();

      return ResponseHandler.success(newUser, 'Signed up successfully');
    } else {
      return ResponseHandler.fail('User already registered');
    }
  }

  async getUserDetailsForAdmin(userId) {
    const user = await this.userModel
      .aggregate([
        {
          $match: {
            _id: mongoose.Types.ObjectId(userId)
          }
        },
        {
          $lookup: {
            from: 'subscriptions',
            let: { userId: '$_id' },
            pipeline: [
              {
                $match: {
                  $expr: {
                    $and: ['$_id', '$$userId']
                  }
                }
              },
              {
                $lookup: {
                  from: 'packages',
                  localField: 'package',
                  foreignField: '_id',
                  as: 'package'
                }
              },
              {
                $lookup: {
                  from: 'blog-categories',
                  localField: 'additionalCategories',
                  foreignField: '_id',
                  as: 'additionalCategories'
                }
              },
              {
                $unwind: '$package'
              },
              {
                $sort: {
                  createdAt: -1
                }
              }
            ],
            as: 'subscriptions'
          }
        },
        {
          $project: {
            email: 1,
            fullName: 1,
            state: 1,
            zip: 1,
            country: 1,
            subscriptionValidTill: 1,
            subscriptionValidFrom: 1,
            isSuspended: 1,
            address: 1,
            city: 1,
            subscriptions: 1
          }
        }
      ])
      .exec();

    if (user && user.length > 0) {
      return ResponseHandler.success(user[0]);
    } else {
      return ResponseHandler.fail('User not found.');
    }
  }

  async getUsers(params: UsersListDto) {
    const { keyword, page, perPage, sortOrder, sortBy } = params;

    const sort = {};
    sort[sortBy] = sortOrder;

    const pipeline = [];

    pipeline.push(
      ...[
        {
          $match: {
            fullName: {
              $regex: new RegExp(keyword),
              $options: 'i'
            }
          }
        },
        {
          $sort: sort
        }
      ]
    );

    const users = await this.userModel
      .aggregate(pipeline)
      .collation({ locale: 'en', strength: 2 })
      .skip((page - 1) * perPage)
      .limit(perPage)
      .exec();

    const { data: rows } = await this.getUserRows(params);

    return ResponseHandler.success({
      users,
      rows
    });
  }

  async getUserRows(params: UsersListDto) {
    const { keyword } = params;

    const rows = await this.userModel
      .countDocuments({
        fullName: {
          $regex: new RegExp(keyword),
          $options: 'i'
        }
      })
      .exec();

    return ResponseHandler.success(rows);
  }

  async getUserById(id, lean = false, select = '') {
    let user = null;
    if (select) {
      user = this.userModel.findById(id, select);
    } else {
      user = this.userModel.findById(id);
    }

    if (lean) {
      return ResponseHandler.success(await user.lean());
    } else {
      return ResponseHandler.success(await user.exec());
    }
  }

  async setSuspended(id: string, isSuspended: boolean) {
    const newUser = await this.userModel
      .findByIdAndUpdate(
        id,
        {
          $set: {
            isSuspended
          }
        },
        {
          new: true
        }
      )
      .lean();

    if (newUser) {
      return ResponseHandler.success(newUser, `User ${isSuspended ? 'suspended' : 'un suspended'} successfully.`);
    } else {
      return ResponseHandler.fail('User not found.');
    }
  }

  async getUserByApiKey(apiKey, lean = true, select = '') {
    let user = null;
    if (select) {
      user = this.userModel.findOne({ apiKey }, select);
    } else {
      user = this.userModel.findOne({ apiKey });
    }

    if (lean) {
      return ResponseHandler.success(await user.lean());
    } else {
      return ResponseHandler.success(await user.exec());
    }
  }

  async validateUserForLogin(authCredentialsDto: AuthCredentialsDto) {
    const { email, password } = authCredentialsDto;
    const user = await this.userModel.findOne({ email }).select('+password +apiKey').lean().exec();
    if (user && user.invitation !== 'pending' && !user.isSuspended && (await compareHash(password, user.password))) {
      delete user.password;
      return user;
    }
    return null;
  }
}
