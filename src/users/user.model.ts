import * as mongoose from 'mongoose';

export const UserSchema = new mongoose.Schema(
  {
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    fullName: { type: String },
    password: { type: String, required: true, select: false },
    email: { type: String, required: true },
    subscriptionValidTill: { type: Date, default: null },
    subscriptionValidFrom: { type: Date, default: null },
    authorizeNetId: { type: String, default: null, select: false },
    apiKey: { type: String, default: null },
    apiKeyUUID: { type: String, default: null, select: false },
    isSuspended: { type: Boolean, default: false },
    address: String,
    country: String,
    state: {
      longName: String,
      shortName: String
    },
    city: String,
    zip: String
  },
  {
    timestamps: true
  }
);
