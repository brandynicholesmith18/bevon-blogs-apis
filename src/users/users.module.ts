import { forwardRef, Module } from '@nestjs/common';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { MongooseModule } from '@nestjs/mongoose';
import { UserSchema } from './user.model';
import { AuthorizeNetModule } from '../authorize-net/authorize-net.module';
import { UsersAdminController } from './users-admin.controller';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'users', schema: UserSchema }]), forwardRef(() => AuthorizeNetModule)],
  controllers: [UsersController, UsersAdminController],
  providers: [UsersService],
  exports: [UsersService]
})
export class UsersModule {}
