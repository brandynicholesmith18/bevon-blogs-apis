import { ApiHideProperty, ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, MinLength, ValidateNested } from 'class-validator';
import responseMessages from '../../config/responseMessages';
import { Type } from 'class-transformer';

export class State {
  @IsNotEmpty({ message: responseMessages.common.requiredState.longName })
  longName?: string;

  @IsNotEmpty({ message: responseMessages.common.requiredState.shortName })
  shortName?: string;
}

export class CreateUserDto {
  @ApiProperty()
  @IsNotEmpty({ message: responseMessages.common.requiredFirstName })
  firstName?: string;

  @ApiProperty()
  @IsNotEmpty({ message: responseMessages.common.requiredLastName })
  lastName?: string;

  @ApiProperty()
  @IsEmail({}, { message: responseMessages.common.invalidEmail })
  email?: string;

  @ApiProperty({ format: 'password' })
  @MinLength(6, { message: responseMessages.common.invalidPassword })
  password?: string;

  // @ApiProperty()
  // @IsNotEmpty({ message: responseMessages.common.requiredAddress })
  // address?: string;

  // @ApiProperty()
  // @IsNotEmpty({ message: responseMessages.common.requiredZip })
  // zip?: string;

  // @ApiProperty()
  // @IsNotEmpty({ message: responseMessages.common.requiredPhone })
  // phone?: string;

  // @ApiProperty()
  // @IsNotEmpty({ message: responseMessages.common.requiredCity })
  // city?: string;

  // @ApiProperty()
  // @IsNotEmpty({ message: responseMessages.common.requiredCountry })
  // country?: string;

  // @ApiProperty({ example: { longName: 'California', shortName: 'CA' } })
  // @ValidateNested()
  // @Type(() => State)
  // state: State;

  @ApiHideProperty()
  _id?: string;
  @ApiHideProperty()
  fullName?: string;
  @ApiHideProperty()
  authorizeNetId?: string;
  @ApiHideProperty()
  apiKey?: string;
  @ApiHideProperty()
  apiKeyUUID?: string;
}
