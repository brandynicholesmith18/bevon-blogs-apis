import { Controller, Get, HttpCode, Param, Post, Query, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiConsumes, ApiOperation, ApiTags } from '@nestjs/swagger';
import { UsersService } from './users.service';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from '../auth/roles.guard';
import { Roles } from '../auth/roles.decorator';
import { UsersListDto } from './dto/users-list.dto';
import { UserIdDto } from '../common/dto/user-id.dto';

@ApiTags('Admin Portal - Users')
@Controller('/admin-users')
export class UsersAdminController {
  constructor(private readonly usersService: UsersService) {}

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles('superAdmin', 'admin')
  @Get()
  async getUsers(@Query() usersListDto: UsersListDto) {
    usersListDto.keyword = usersListDto.keyword ? usersListDto.keyword : '';

    return await this.usersService.getUsers(usersListDto);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles('superAdmin', 'admin')
  @Get('/details/:id')
  async getUserDetails(@Param() userIdDto: UserIdDto) {
    return await this.usersService.getUserDetailsForAdmin(userIdDto.id);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles('superAdmin')
  @ApiOperation({ summary: 'Suspend user.' })
  @Post('/suspend/:id')
  @HttpCode(200)
  @ApiConsumes('application/x-www-form-urlencoded')
  async suspendAdmin(@Param() userIdDto: UserIdDto) {
    return await this.usersService.setSuspended(userIdDto.id, true);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles('superAdmin')
  @ApiOperation({ summary: 'Unsuspend user.' })
  @Post('/un-suspend/:id')
  @HttpCode(200)
  @ApiConsumes('application/x-www-form-urlencoded')
  async unSuspendAdmin(@Param() userIdDto: UserIdDto) {
    return await this.usersService.setSuspended(userIdDto.id, false);
  }
}
