import { IsNumber } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import responseMessages from '../../config/responseMessages';

export class PaginationDto {
  @ApiProperty({ default: 1 })
  @IsNumber({}, { message: responseMessages.common.requiredPage })
  page: number;

  @ApiProperty({ default: 10 })
  @IsNumber({}, { message: responseMessages.common.requiredPerPage })
  perPage: number;
}
