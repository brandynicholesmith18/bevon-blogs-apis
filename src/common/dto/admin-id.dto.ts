import { IsMongoId } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class AdminIdDto {
  @ApiProperty()
  @IsMongoId({ message: 'A valid mongodb admin id is required.' })
  id: string;
}
