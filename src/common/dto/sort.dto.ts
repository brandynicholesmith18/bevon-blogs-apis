import { IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import responseMessages from '../../config/responseMessages';

export class SortDto {
  @ApiProperty()
  @IsNotEmpty({message: responseMessages.common.requiredSortBy})
  sortBy?: string;

  @ApiProperty()
  @IsNotEmpty({message: responseMessages.common.requiredSortOrder})
  sortOrder?: string;
}
