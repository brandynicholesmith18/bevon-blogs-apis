import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber } from 'class-validator';
import responseMessages from '../../config/responseMessages';

export class ListDto {
  @ApiProperty({ required: false })
  keyword?: string;

  @ApiProperty({ default: 1 })
  @IsNumber({}, { message: responseMessages.common.requiredPage })
  page?: number;

  @ApiProperty({ default: 10 })
  @IsNumber({}, { message: responseMessages.common.requiredPerPage })
  perPage?: number;

  @ApiProperty({ default: 'createdAt' })
  @IsNotEmpty({ message: responseMessages.common.requiredSortBy })
  sortBy?: string;

  @ApiProperty({ default: -1 })
  @IsNumber({}, { message: responseMessages.common.requiredSortOrder })
  sortOrder?: number;
}
