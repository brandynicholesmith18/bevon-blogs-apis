import { IsMongoId } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class PackageIdDto {
  @ApiProperty()
  @IsMongoId({ message: 'A valid mongodb package id is required.' })
  id: string;
}
