import { ApiProperty } from '@nestjs/swagger';

export class PriceDto {
  @ApiProperty()
  price?: number;
}
