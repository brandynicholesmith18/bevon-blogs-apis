import { IsMongoId } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import responseMessages from '../../config/responseMessages';

export class BlogIdDto {
  @ApiProperty()
  @IsMongoId({ message: responseMessages.common.invalidBlogId })
  id: string;
}
