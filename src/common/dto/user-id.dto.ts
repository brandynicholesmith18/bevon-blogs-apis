import { IsMongoId } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class UserIdDto {
  @ApiProperty()
  @IsMongoId({ message: 'A valid mongodb user id is required.' })
  id: string;
}
