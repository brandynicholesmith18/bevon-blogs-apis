import { IsDateString, MaxLength } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CardDto {
  @ApiProperty()
  number?: string;

  @ApiProperty()
  @MaxLength(3)
  code?: string;

  @ApiProperty()
  expiryDate?: string;
}
