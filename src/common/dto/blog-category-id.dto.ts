import { IsMongoId } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import responseMessages from '../../config/responseMessages';

export class BlogCategoryIdDto {
  @ApiProperty()
  @IsMongoId({ message: responseMessages.common.invalidBlogCategoryId })
  id: string;
}
