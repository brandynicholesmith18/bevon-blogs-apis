import { AUTHORIZE_NET_MODE } from '../config/config';
import authorizeNet = require('authorizenet');
import bcrypt = require('bcryptjs');

const ApiContracts = authorizeNet.APIContracts;

const saltRounds = 13;

export const compareHash = async (password: string | undefined, hash: string | undefined): Promise<boolean> => {
  return bcrypt.compare(password, hash);
};

export const getHash = async (password: string | undefined): Promise<string> => {
  return await bcrypt.hash(password, saltRounds);
};

export const getAuthorizeMode = () => {
  return AUTHORIZE_NET_MODE === 'TESTMODE' ? ApiContracts.ValidationModeEnum.TESTMODE : ApiContracts.ValidationModeEnum.LIVEMODE;
};

export const getSlug = (str: string, id: number = null) => {
  str = str.toLowerCase().replace(/ /g, '-');
  let val = '';
  for (let i = 0; i < str.length; i++) {
    if (
      str[i] === '-' ||
      (str.charCodeAt(i) >= 48 && str.charCodeAt(i) <= 57) ||
      (str.charCodeAt(i) >= 65 && str.charCodeAt(i) <= 90) ||
      (str.charCodeAt(i) >= 97 && str.charCodeAt(i) <= 122)
    ) {
      val += str[i];
    }
  }

  return id ? `${val}-${id}` : val;
};
