import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { MailerService } from '@nestjs-modules/mailer';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService, private readonly mailerService: MailerService) {}

  // @Get()
  index() {
    return 'Hello';
  }

  // @Get('mail-test')
  async sendTestMail() {
    this.mailerService
      .sendMail({
        to: 'musama96@live.com',
        from: process.env.MAILER_USERNAME,
        subject: 'Testing Nest Mailer Module',
        text: 'welcome', // plaintext body
        html: '<b>welcome</b>' // HTML body content
      })
      .then((resp) => {
        return 'Sent';
      })
      .catch((e) => {
        return e;
      });

    return 'Mail test';
  }
}
