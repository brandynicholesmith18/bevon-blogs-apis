import * as mongoose from 'mongoose';

export const NumericIdSchema = new mongoose.Schema(
  {
    blogs: { type: Number, default: 0 },
    blogCategories: { type: Number, default: 0 }
  },
  {
    timestamps: true
  }
);
