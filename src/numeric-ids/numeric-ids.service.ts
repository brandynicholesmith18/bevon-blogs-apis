import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import ResponseHandler from '../common/ResponseHandler';

@Injectable()
export class NumericIdsService {
  constructor(@InjectModel('numeric-ids') private readonly numericIdModel) {}

  async getNextBlogId() {
    const ids = await this.numericIdModel
      .findOneAndUpdate(
        {},
        {
          $inc: {
            blogs: 1
          }
        },
        {
          new: true
        }
      )
      .lean();

    return ResponseHandler.success(ids.blogs);
  }

  async getNextBlogCategoryId() {
    const ids = await this.numericIdModel
      .findOneAndUpdate(
        {},
        {
          $inc: {
            blogCategories: 1
          }
        },
        {
          new: true,
          upsert: true
        }
      )
      .lean();

    return ResponseHandler.success(ids.blogCategories);
  }
}
