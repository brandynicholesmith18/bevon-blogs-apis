import { Module } from '@nestjs/common';
import { NumericIdsService } from './numeric-ids.service';
import { MongooseModule } from '@nestjs/mongoose';
import { NumericIdSchema } from './numeric-id.model';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'numeric-ids', schema: NumericIdSchema }])],
  providers: [NumericIdsService],
  exports: [NumericIdsService]
})
export class NumericIdsModule {}
