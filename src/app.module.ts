import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { BlogsModule } from './blogs/blogs.module';
import { UsersModule } from './users/users.module';
import { AdminsModule } from './admins/admins.module';
import { MongooseModule } from '@nestjs/mongoose';
import { MONGOURI } from './config/config';
import { AuthorizeNetModule } from './authorize-net/authorize-net.module';
import { BlogCategoriesModule } from './blog-categories/blog-categories.module';
import { PackagesModule } from './packages/packages.module';
import { SubscriptionsModule } from './subscriptions/subscriptions.module';
import { AuthModule } from './auth/auth.module';
import { NumericIdsModule } from './numeric-ids/numeric-ids.module';
import { MailerModule } from '@nestjs-modules/mailer';
import { SquareModule } from './square/square.module';

@Module({
  imports: [
    BlogsModule,
    UsersModule,
    AdminsModule,
    AuthModule,
    MongooseModule.forRoot(MONGOURI, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true
    }),
    AuthorizeNetModule,
    BlogCategoriesModule,
    PackagesModule,
    SubscriptionsModule,
    NumericIdsModule,
    MailerModule.forRoot({
      transport: {
        host: process.env.MAILER_HOST,
        port: Number(process.env.MAILER_PORT),
        secure: JSON.parse(process.env.MAILER_SECURE),
        ignoreTLS: true,
        auth: {
          user: process.env.MAILER_USERNAME,
          pass: process.env.MAILER_PASSWORD
        }
      }
    }),
    SquareModule
  ],
  controllers: [AppController],
  providers: [AppService]
})
export class AppModule {}
