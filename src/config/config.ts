import * as dotenv from 'dotenv';

dotenv.config();

export const PORT = process.env.PORT;
export const HOSTED_ON = process.env.HOSTED_ON;
export const MONGOURI = process.env.MONGOURI;
export const JWT_SECRET = process.env.JWT_SECRET;
export const NUMBER_STRING_FIELDS = [];
export const AUTHORIZE_NET_API_LOGIN_ID = process.env.AUTHORIZE_NET_API_LOGIN_ID;
export const AUTHORIZE_NET_TRANSACTION_KEY = process.env.AUTHORIZE_NET_TRANSACTION_KEY;
export const AUTHORIZE_NET_MODE = process.env.AUTHORIZE_NET_MODE;
export const SQUARE_APPLICATION_ID = process.env.SQUARE_APPLICATION_ID;
export const SQUARE_ACCESS_TOKEN = process.env.SQUARE_ACCESS_TOKEN;
export const SQUARE_MODE = process.env.SQUARE_MODE;
