export default {
  success: {},
  common: {
    fieldValidationError: 'Validation failed: Please check individual fields for specific errors',
    requiredFirstName: 'First name is required.',
    requiredLastName: 'Last name is required.',
    requiredAddress: 'Address is required.',
    requiredPhone: 'Phone number is required.',
    requiredCity: 'City is required.',
    requiredZip: 'Zip code is required.',
    requiredState: {
      longName: 'State long name is required.',
      shortName: 'State short name is required.'
    },
    requiredCountry: 'Country is required.',
    requiredSortBy: 'Sort by is required.',
    requiredSortOrder: 'Sort order is required.',
    requiredPage: 'Page number is required.',
    requiredPerPage: 'Per page is required.',
    invalidAdminType: 'Admin type not valid.',
    invalidEmail: 'Email is not valid.',
    invalidPassword: 'Password must be minimum of 6 characters.',
    invalidAdminId: 'Admin id is not valid.',
    invalidBlogCategoryId: 'Blog category id is invalid.',
    invalidBlogId: 'Blog id is invalid.',
    invalidPackageId: 'Package id is invalid.',
    invalidSubscriptionId: 'Subscription id is invalid.',
    invalidUserId: 'User id is invalid'
  },
  createSubscription: {
    additionalCategories: 'Additional categories must be array of mongodb ids.',
    idempotencyKey: 'Idempotency key is required.',
    sourceId: 'Source id is required.'
  },
  createBlog: {
    title: 'Title is required.',
    description: 'Description is required.',
    type: 'Type is invalid.',
    price: 'Price must be a number.',
    featured: 'Featured must be a boolean.'
  },
  authCredentials: {
    invalidEmail: 'Please enter a valid Email',
    emptyPassword: 'Please enter your Password',
    invalidPassword: 'You must enter a password.'
  }
};
