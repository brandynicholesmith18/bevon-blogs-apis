import { Body, Controller, HttpCode, Post, UnauthorizedException } from '@nestjs/common';
import { ApiConsumes, ApiOperation, ApiTags } from '@nestjs/swagger';
import { AuthCredentialsDto } from './dto/authCredentialsDto';
import { AuthService } from './auth.service';
import { UsersService } from '../users/users.service';
import { JwtPayLoad } from './jwt.payload.interface';
import { JwtService } from '@nestjs/jwt';
import ResponseHandler from '../common/ResponseHandler';
import { AdminsService } from '../admins/admins.service';

@Controller('auth')
@ApiTags('Auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private adminsService: AdminsService,
    private usersService: UsersService,
    private readonly jwtService: JwtService
  ) {}

  @ApiOperation({ summary: 'Users login.' })
  @Post('/users/login')
  @HttpCode(200)
  @ApiConsumes('application/x-www-form-urlencoded')
  async userLogin(@Body() authCredentialsDto: AuthCredentialsDto) {
    const user = await this.usersService.validateUserForLogin(authCredentialsDto);
    if (!user) {
      throw new UnauthorizedException('Invalid Credentials');
    }
    return ResponseHandler.success(user);
  }

  @ApiOperation({ summary: 'Admins login.' })
  @Post('/admins/login')
  @HttpCode(200)
  @ApiConsumes('application/x-www-form-urlencoded')
  async adminLogin(@Body() authCredentialsDto: AuthCredentialsDto) {
    const admin = await this.adminsService.validateAdminForLogin(authCredentialsDto);
    if (!admin) {
      throw new UnauthorizedException('Invalid Credentials');
    }
    const payload: JwtPayLoad = { _id: admin._id, email: admin.email, type: 'admin' };
    const accessToken = this.jwtService.sign(payload);
    return ResponseHandler.success({ ...admin, accessToken });
  }
}
