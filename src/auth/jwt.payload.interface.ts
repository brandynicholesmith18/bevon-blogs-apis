export interface JwtPayLoad {
  _id?: string;
  email: string;
  type: string;
}

export interface CustomerJwtPayLoad {
  _id?: string;
  email: string;
  customerStripeId: string;
}
