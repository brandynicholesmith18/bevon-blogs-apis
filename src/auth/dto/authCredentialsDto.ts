import { IsEmail, IsNotEmpty, IsString, Matches, MinLength } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import responseMessages from 'src/config/responseMessages';

export class AuthCredentialsDto {
  @ApiProperty({ example: 'musama96@live.com', description: 'Enter email for logging in.' })
  @IsEmail({}, { message: responseMessages.authCredentials.invalidEmail })
  email: string;

  @ApiProperty({ default: 'D945gccr', format: 'password', description: 'Enter password for logging in.' })
  @IsString({ message: responseMessages.authCredentials.invalidPassword })
  password: string;
}
