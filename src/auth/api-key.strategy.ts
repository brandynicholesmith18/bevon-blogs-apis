import { HeaderAPIKeyStrategy } from 'passport-headerapikey';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';

@Injectable()
export class ApiKeyStrategy extends PassportStrategy(HeaderAPIKeyStrategy) {
  constructor(private readonly usersService: UsersService) {
    super({ header: 'API-KEY', prefix: '' }, true, async (apiKey, done, req) => {
      const { data: user } = await this.usersService.getUserByApiKey(apiKey, true, '+authorizeNetId');
      if (!user) {
        return done(false);
      }
      return done(null, user);
    });
  }
}
