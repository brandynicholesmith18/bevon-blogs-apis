import { JWT_SECRET } from '../config/config';

export const jwtConstants = {
  secret: JWT_SECRET
};
