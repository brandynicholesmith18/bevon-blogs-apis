import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { jwtConstants } from './constants';
import { JwtPayLoad } from './jwt.payload.interface';
import { AdminsService } from '../admins/admins.service';
import { UsersService } from '../users/users.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly adminsService: AdminsService, private readonly usersService: UsersService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: jwtConstants.secret
    });
  }

  async validate(payload: JwtPayLoad) {
    const { _id } = payload;

    const { data: admin } = await this.adminsService.getAdminById(_id, true);

    if (!admin) {
      const { data: user } = await this.usersService.getUserById(_id, true);
      user.role = 'user';

      if (!user) {
        throw new UnauthorizedException();
      }
      return user;
    }
    return admin;
  }
}
