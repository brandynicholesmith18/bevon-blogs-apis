import { forwardRef, Module } from '@nestjs/common';
import { BlogCategoriesService } from './blog-categories.service';
import { BlogCategoriesController } from './blog-categories.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { BlogCategorySchema } from './blog-category.model';
import { NumericIdsModule } from '../numeric-ids/numeric-ids.module';
import { BlogCategoriesAdminController } from './blog-categories-admin.controller';
import { SubscriptionsModule } from 'src/subscriptions/subscriptions.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'blog-categories', schema: BlogCategorySchema }]),
    NumericIdsModule,
    forwardRef(() => SubscriptionsModule)
  ],
  providers: [BlogCategoriesService],
  controllers: [BlogCategoriesController, BlogCategoriesAdminController],
  exports: [BlogCategoriesService]
})
export class BlogCategoriesModule {}
