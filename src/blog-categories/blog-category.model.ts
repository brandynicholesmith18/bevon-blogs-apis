import * as mongoose from 'mongoose';

export const BlogCategorySchema = new mongoose.Schema(
  {
    title: { type: String },
    type: { type: String, enum: ['basic', 'additional'] },
    numId: { type: Number, unique: true },
    description: { type: String, default: null },
    featured: { type: Boolean, default: false },
    price: { type: Number, default: 0 },
    deletedAt: { type: Date, default: null }
  },
  {
    timestamps: true
  }
);
