import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { BlogCategoriesListDto } from './dto/blog-categories-list.dto';
import { CreateBlogCategoryDto } from './dto/create-blog-category.dto';
import { NumericIdsService } from '../numeric-ids/numeric-ids.service';
import { getSlug } from '../common/functions';
import ResponseHandler from '../common/ResponseHandler';

@Injectable()
export class BlogCategoriesService {
  constructor(@InjectModel('blog-categories') private readonly blogCategoryModel, private readonly numericIdsService: NumericIdsService) {}

  async getBlogCategoriesForUser(additionalCategoryIds: string[]) {
    const categories = await this.blogCategoryModel
      .find({
        $or: [{ type: 'basic' }, { type: 'additional', _id: additionalCategoryIds }],
        deletedAt: null
      })
      .lean();

    return ResponseHandler.success(categories);
  }

  async createBlogCategory(blogCategory: CreateBlogCategoryDto) {
    const check = await this.blogCategoryModel
      .findOne({
        title: blogCategory.title
      })
      .lean();

    if (check) {
      return ResponseHandler.fail('Could not create blog category. A category with this name already exists.');
    }

    const { data: numId } = await this.numericIdsService.getNextBlogCategoryId();
    const slug = getSlug(blogCategory.title, numId);

    blogCategory.numId = numId;
    blogCategory.slug = slug;

    let newBlogCategory = await this.blogCategoryModel(blogCategory);
    newBlogCategory = await newBlogCategory.save();

    return ResponseHandler.success(newBlogCategory, 'Blog category created successfully.');
  }

  async updateBlogCategory(id: string, blogCategory: CreateBlogCategoryDto) {
    const check = await this.blogCategoryModel
      .findOne({
        title: blogCategory.title
      })
      .lean();

    if (check && check._id.toString() !== id) {
      return ResponseHandler.fail('Could not update blog category. A category with this name already exists.');
    }

    blogCategory.slug = getSlug(blogCategory.title, blogCategory.numId);
    delete blogCategory._id;

    const newBlogCategory = await this.blogCategoryModel
      .findByIdAndUpdate(
        id,
        {
          $set: blogCategory
        },
        {
          new: true
        }
      )
      .lean();

    return ResponseHandler.success(newBlogCategory, 'Blog category created successfully.');
  }

  async getAllBlogCategories(keyword) {
    const pipeline = [];

    pipeline.push(
      ...[
        {
          $match: {
            title: {
              $regex: new RegExp(keyword),
              $options: 'i'
            },
            deletedAt: null
          }
        },
        {
          $sort: {
            createdAt: -1
          }
        }
      ]
    );

    const categories = await this.blogCategoryModel.aggregate(pipeline).collation({ locale: 'en', strength: 2 }).exec();

    return ResponseHandler.success(categories);
  }

  async getBlogCategories(params: BlogCategoriesListDto) {
    const { keyword, sortOrder, sortBy, page, perPage, paginate, deleted } = params;

    const sort = {};
    sort[sortBy] = sortOrder;

    const match = {
      title: {
        $regex: new RegExp(keyword),
        $options: 'i'
      },
      deletedAt: null
    };

    if (!deleted) {
      delete match.deletedAt;
    }

    const pipeline = [];

    pipeline.push({ $match: match });

    const rowsPipeline = [...pipeline];

    pipeline.push({ $sort: sort });
    rowsPipeline.push({ $group: { _id: null, rows: { $sum: 1 } } });

    let categories = this.blogCategoryModel.aggregate(pipeline).collation({ locale: 'en', strength: 2 });

    const rows = await this.blogCategoryModel.aggregate(rowsPipeline).exec();

    if (paginate) {
      categories = categories.skip((page - 1) * perPage).limit(perPage);
    }

    categories = await categories.exec();

    return ResponseHandler.success({
      categories,
      rows: rows.length > 0 ? rows[0].rows : 0
    });
  }

  async getBlogCategoriesForUsers(params: BlogCategoriesListDto, subscription) {
    const { data: cats } = await this.getBlogCategoriesForUser(subscription.additionalCategories);
    const validCatIds = cats.map((category) => category._id);

    const { keyword, sortOrder, sortBy, page, perPage, paginate, deleted } = params;

    const sort = {};
    sort[sortBy] = sortOrder;

    const match = {
      title: {
        $regex: new RegExp(keyword),
        $options: 'i'
      },
      _id: { $in: validCatIds },
      deletedAt: null
    };

    if (!deleted) {
      delete match.deletedAt;
    }

    const pipeline = [];

    pipeline.push({ $match: match });

    const rowsPipeline = [...pipeline];

    pipeline.push({ $sort: sort });
    rowsPipeline.push({ $group: { _id: null, rows: { $sum: 1 } } });

    let categories = this.blogCategoryModel.aggregate(pipeline).collation({ locale: 'en', strength: 2 });

    const rows = await this.blogCategoryModel.aggregate(rowsPipeline).exec();

    if (paginate) {
      categories = categories.skip((page - 1) * perPage).limit(perPage);
    }

    categories = await categories.exec();

    return ResponseHandler.success({
      categories,
      rows: rows.length > 0 ? rows[0].rows : 0
    });
  }

  async toggleDelete(id) {
    const category = await this.blogCategoryModel.findById(id).exec();

    if (category) {
      category.deletedAt = category.deletedAt ? null : new Date();
      await category.save();
      return ResponseHandler.success(category, `Category ${category.deletedAt ? 'disabled' : 'enabled'} successfully.`);
    } else {
      return ResponseHandler.fail('Blog category not found.');
    }
  }

  async getBlogCategoriesByIds(ids: string[]) {
    const categories = await this.blogCategoryModel.find({ _id: ids }).lean();

    return ResponseHandler.success(categories);
  }
}
