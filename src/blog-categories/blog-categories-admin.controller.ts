import { Body, Controller, Get, HttpCode, Param, Post, Query, UseGuards } from '@nestjs/common';
import { BlogCategoriesService } from './blog-categories.service';
import { ApiBearerAuth, ApiConsumes, ApiOperation, ApiTags } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from '../auth/roles.guard';
import { Roles } from '../auth/roles.decorator';
import { CreateBlogCategoryDto } from './dto/create-blog-category.dto';
import { BlogCategoryIdDto } from '../common/dto/blog-category-id.dto';
import { BlogCategoriesListDto } from './dto/blog-categories-list.dto';

@ApiTags('Admin Portal - Blog Categories')
@Controller('/admin-blog-categories')
export class BlogCategoriesAdminController {
  constructor(private readonly blogCategoriesService: BlogCategoriesService) {}

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles('superAdmin', 'admin')
  @Get()
  @ApiOperation({ description: 'Get blog categories.' })
  async getBlogCategories(@Query() blogCategoriesListDto: BlogCategoriesListDto) {
    return await this.blogCategoriesService.getBlogCategories(blogCategoriesListDto);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles('superAdmin', 'admin')
  @Get('all')
  @ApiOperation({ description: 'Get all blog categories.' })
  async getAllBlogCategories(@Query('keyword') keyword) {
    return await this.blogCategoriesService.getAllBlogCategories(keyword);
  }

  @ApiBearerAuth()
  @ApiOperation({ description: 'Create new blog category.' })
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles('superAdmin')
  @Post()
  @HttpCode(200)
  @ApiConsumes('application/x-www-form-urlencoded')
  async createBlogCategory(@Body() createBlogCategoryDto: CreateBlogCategoryDto) {
    return await this.blogCategoriesService.createBlogCategory(createBlogCategoryDto);
  }

  @ApiBearerAuth()
  @ApiOperation({ description: 'Update a blog category.' })
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles('superAdmin')
  @Post('/edit/:id')
  @HttpCode(200)
  @ApiConsumes('application/x-www-form-urlencoded')
  async updateBlogCategory(@Body() createBlogCategoryDto: CreateBlogCategoryDto, @Param() blogCategoryIdDto: BlogCategoryIdDto) {
    return await this.blogCategoriesService.updateBlogCategory(blogCategoryIdDto.id, createBlogCategoryDto);
  }

  @ApiBearerAuth()
  @ApiOperation({ description: 'Toggle category delete.' })
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles('superAdmin')
  @Post('/toggle-deleted/:id')
  @HttpCode(200)
  @ApiConsumes('application/x-www-form-urlencoded')
  async toggleDelete(@Param() blogCategoryIdDto: BlogCategoryIdDto) {
    return await this.blogCategoriesService.toggleDelete(blogCategoryIdDto.id);
  }
}
