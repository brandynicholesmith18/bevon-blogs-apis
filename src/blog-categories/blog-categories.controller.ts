import { Controller, Get, Query, UseGuards } from '@nestjs/common';
import { BlogCategoriesService } from './blog-categories.service';
import { BlogCategoriesListDto } from './dto/blog-categories-list.dto';
import { ApiOperation, ApiSecurity, ApiTags } from '@nestjs/swagger';
import { GetUser } from 'src/auth/get-user.decorator';
import { AuthGuard } from '@nestjs/passport';
import { SubscriptionsService } from 'src/subscriptions/subscriptions.service';
import ResponseHandler from 'src/common/ResponseHandler';

@ApiTags('User Portal - Blog Categories')
@Controller('blog-categories')
export class BlogCategoriesController {
  constructor(private readonly blogCategoriesService: BlogCategoriesService, private readonly subscriptionsService: SubscriptionsService) {}

  @Get()
  @ApiSecurity('API-KEY', ['API-KEY'])
  @UseGuards(AuthGuard('headerapikey'))
  @ApiOperation({ description: 'Get blog categories.' })
  async getBlogCategories(@Query() blogCategoriesListDto: BlogCategoriesListDto, @GetUser() user) {
    const { data: subscription } = await this.subscriptionsService.getCurrentlyValidUserSubscription(user._id);

    if (subscription) {
      blogCategoriesListDto.keyword = blogCategoriesListDto.keyword ? blogCategoriesListDto.keyword : '';

      return await this.blogCategoriesService.getBlogCategoriesForUsers(blogCategoriesListDto, subscription);
    } else {
      return ResponseHandler.success({ categories: [], rows: 0 });
    }
  }

  @Get('all')
  @ApiOperation({ description: 'Get all blog categories.' })
  async getAllBlogCategories(@Query('keyword') keyword: string) {
    return await this.blogCategoriesService.getAllBlogCategories(keyword ? keyword : '');
  }
}
