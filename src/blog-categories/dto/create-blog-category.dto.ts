import { ApiHideProperty, ApiProperty } from '@nestjs/swagger';
import { IsIn, IsNotEmpty, IsNumber, IsOptional } from 'class-validator';
import responseMessages from '../../config/responseMessages';

export class CreateBlogCategoryDto {
  @ApiProperty()
  @IsNotEmpty({ message: responseMessages.createBlog.title })
  title?: string;

  @ApiProperty()
  @IsNotEmpty({ message: responseMessages.createBlog.description })
  description?: string;

  @ApiProperty()
  @IsIn(['basic', 'additional'], { message: responseMessages.createBlog.type })
  type?: string;

  @ApiProperty()
  price?: number;

  @ApiHideProperty()
  // @IsBoolean({ message: responseMessages.createBlog.featured })
  featured?: string;
  @ApiHideProperty()
  numId?: number;
  @ApiHideProperty()
  slug?: string;
  @ApiHideProperty()
  _id?: string;
}
