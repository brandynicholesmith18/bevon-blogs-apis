import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean } from 'class-validator';
import { ListDto } from '../../common';

export class BlogCategoriesListDto extends ListDto {
  @ApiProperty({ required: false })
  @IsBoolean()
  paginate: boolean;

  @ApiProperty({ required: false })
  @IsBoolean()
  deleted: boolean;
}
