import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import responseMessages from '../../config/responseMessages';

export class UpdatePasswordDto {
  @ApiProperty()
  @IsNotEmpty({ message: responseMessages.common.invalidPassword })
  oldPassword?: string;

  @ApiProperty()
  @IsNotEmpty({ message: responseMessages.common.invalidPassword })
  newPassword?: string;
}
