import { ApiHideProperty, ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsIn, IsNotEmpty } from 'class-validator';
import responseMessages from '../../config/responseMessages';

export class UpdateAdminDto {
  @ApiProperty()
  @IsNotEmpty({ message: responseMessages.common.requiredFirstName })
  firstName?: string;

  @ApiProperty()
  @IsNotEmpty({ message: responseMessages.common.requiredLastName })
  lastName?: string;

  @ApiProperty()
  @IsIn(['superAdmin', 'admin'], { message: responseMessages.common.invalidAdminType })
  role?: string;

  @ApiHideProperty()
  fullName?: string;
}
