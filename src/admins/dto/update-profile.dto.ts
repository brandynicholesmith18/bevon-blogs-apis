import { ApiHideProperty, ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import responseMessages from '../../config/responseMessages';

export class UpdateProfileDto {
  @ApiProperty()
  @IsNotEmpty({ message: responseMessages.common.requiredFirstName })
  firstName?: string;

  @ApiProperty()
  @IsNotEmpty({ message: responseMessages.common.requiredLastName })
  lastName?: string;

  @ApiHideProperty()
  fullName?: string;
}
