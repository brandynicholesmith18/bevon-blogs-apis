import { ApiHideProperty, ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsIn, IsNotEmpty } from 'class-validator';
import responseMessages from '../../config/responseMessages';

export class State {
  @IsNotEmpty({ message: responseMessages.common.requiredState.longName })
  longName?: string;

  @IsNotEmpty({ message: responseMessages.common.requiredState.shortName })
  shortName?: string;
}

export class CreateAdminDto {
  @ApiProperty()
  @IsNotEmpty({ message: responseMessages.common.requiredFirstName })
  firstName?: string;

  @ApiProperty()
  @IsNotEmpty({ message: responseMessages.common.requiredLastName })
  lastName?: string;

  @ApiProperty()
  @IsEmail({}, { message: responseMessages.common.invalidEmail })
  email?: string;

  @ApiProperty()
  @IsIn(['superAdmin', 'admin'], { message: responseMessages.common.invalidAdminType })
  role?: string;

  @ApiHideProperty()
  password?: string;
  @ApiHideProperty()
  _id?: string;
  @ApiHideProperty()
  fullName?: string;
  @ApiHideProperty()
  authorizeNetId?: string;
}
