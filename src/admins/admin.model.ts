import * as mongoose from 'mongoose';

export const AdminSchema = new mongoose.Schema(
  {
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    fullName: { type: String },
    email: { type: String, required: true },
    password: { type: String, required: true, select: false },
    role: { type: String, enum: ['superAdmin', 'admin'] },
    isVerified: { type: Boolean, default: false },
    isSuspended: { type: Boolean, default: false },
    deletedAt: { type: Date, default: null }
  },
  {
    timestamps: true
  }
);
