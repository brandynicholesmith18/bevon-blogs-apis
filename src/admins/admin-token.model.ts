import * as mongoose from 'mongoose';

export const AdminTokenSchema = new mongoose.Schema({
  adminId: { type: mongoose.Schema.Types.ObjectId, ref: 'admins' },
  token: { type: String, required: true },
  date: { type: Date, required: true, default: Date.now, expires: 1296000 },
});

export interface AdminToken extends mongoose.Document {
  adminId: string;
  token: string;
}
