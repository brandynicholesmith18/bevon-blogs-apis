import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateAdminDto, AdminsListDto, UpdateAdminDto } from './dto';
import { AuthCredentialsDto } from '../auth/dto/authCredentialsDto';
import { compareHash, getHash } from '../common/functions';
import { UpdatePasswordDto } from './dto/update-password.dto';
import { UpdateProfileDto } from './dto/update-profile.dto';
import ResponseHandler from '../common/ResponseHandler';

@Injectable()
export class AdminsService {
  constructor(@InjectModel('admins') private readonly adminModel) {}

  async createAdmin(admin: CreateAdminDto) {
    const check = await this.adminModel.findOne({ email: admin.email }).lean();

    if (!check) {
      admin.password = await getHash(Math.random().toString(36).substring(7));
      admin.fullName = `${admin.firstName} ${admin.lastName}`;
      let newAdmin = new this.adminModel(admin);
      newAdmin = await newAdmin.save();
      delete newAdmin._doc.password;

      return ResponseHandler.success(newAdmin, 'Signed up successfully');
    } else {
      return ResponseHandler.fail('Admin already registered');
    }
  }

  async updatePassword(id, { oldPassword, newPassword }: UpdatePasswordDto) {
    const admin = await this.adminModel.findById(id).select('+password').exec();

    if (await compareHash(oldPassword, admin.password)) {
      admin.password = await getHash(newPassword);
      await admin.save();

      return ResponseHandler.success(await this.adminModel.findById(id).lean(), 'Password updated successfully');
    } else {
      return ResponseHandler.fail('Invalid old password');
    }
  }

  async updateAdmin(id: string, admin: UpdateAdminDto) {
    const newAdmin = await this.adminModel
      .findByIdAndUpdate(
        id,
        {
          $set: {
            fullName: `${admin.firstName} ${admin.lastName}`,
            firstName: admin.firstName,
            lastName: admin.lastName,
            role: admin.role
          }
        },
        {
          new: true
        }
      )
      .lean();

    if (newAdmin) {
      return ResponseHandler.success(newAdmin, 'Admin updated successfully.');
    } else {
      return ResponseHandler.fail('Admin not found.');
    }
  }

  async updateProfile(id: string, admin: UpdateProfileDto) {
    const newAdmin = await this.adminModel
      .findByIdAndUpdate(
        id,
        {
          $set: {
            fullName: `${admin.firstName} ${admin.lastName}`,
            firstName: admin.firstName,
            lastName: admin.lastName
          }
        },
        {
          new: true
        }
      )
      .lean();

    return ResponseHandler.success(newAdmin, 'Profile updated successfully.');
  }

  async setSuspended(id: string, isSuspended: boolean) {
    const newAdmin = await this.adminModel
      .findByIdAndUpdate(
        id,
        {
          $set: {
            isSuspended
          }
        },
        {
          new: true
        }
      )
      .lean();

    if (newAdmin) {
      return ResponseHandler.success(newAdmin, `Admin ${isSuspended ? 'suspended' : 'un suspended'} successfully.`);
    } else {
      return ResponseHandler.fail('Admin not found.');
    }
  }

  async getAdmins(params: AdminsListDto) {
    const { keyword, page, perPage, sortOrder, sortBy } = params;

    const sort = {};
    sort[sortBy] = sortOrder;

    const pipeline = [];

    pipeline.push(
      ...[
        {
          $match: {
            fullName: {
              $regex: new RegExp(keyword),
              $options: 'i'
            }
          }
        },
        {
          $sort: sort
        }
      ]
    );

    const admins = await this.adminModel
      .aggregate(pipeline)
      .collation({ locale: 'en', strength: 2 })
      .skip((page - 1) * perPage)
      .limit(perPage)
      .exec();

    const { data: rows } = await this.getAdminRows(params);

    return ResponseHandler.success({
      admins,
      rows
    });
  }

  async getAdminRows(params: AdminsListDto) {
    const { keyword } = params;

    const rows = await this.adminModel
      .countDocuments({
        fullName: {
          $regex: new RegExp(keyword),
          $options: 'i'
        }
      })
      .exec();

    return ResponseHandler.success(rows);
  }

  async getAdminById(id, lean = false) {
    const admin = this.adminModel.findById(id);
    if (lean) {
      return ResponseHandler.success(await admin.lean());
    } else {
      return ResponseHandler.success(await admin.exec());
    }
  }

  async validateAdminForLogin(authCredentialsDto: AuthCredentialsDto) {
    const { email, password } = authCredentialsDto;
    const admin = await this.adminModel.findOne({ email }).select('+password').lean().exec();
    if (admin && admin.isVerified && !admin.isSuspended && (await compareHash(password, admin.password))) {
      delete admin.password;
      return admin;
    }
    return null;
  }
}
