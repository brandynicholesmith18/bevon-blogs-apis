import { Body, Controller, Get, HttpCode, Param, Post, Query, UseGuards } from '@nestjs/common';
import { AdminsService } from './admins.service';
import { ApiBearerAuth, ApiConsumes, ApiOperation, ApiTags } from '@nestjs/swagger';
import { CreateAdminDto, UpdateAdminDto, AdminsListDto } from './dto';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from '../auth/roles.guard';
import { Roles } from '../auth/roles.decorator';
import { AdminIdDto } from '../common/dto/admin-id.dto';
import { GetUser } from '../auth/get-user.decorator';
import { UpdatePasswordDto } from './dto/update-password.dto';
import { UpdateProfileDto } from './dto/update-profile.dto';

@ApiTags('Admin Portal - Admins')
@Controller('admins')
export class AdminsController {
  constructor(private readonly adminsService: AdminsService) {}

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles('superAdmin')
  @ApiOperation({ summary: 'Create new admin.' })
  @Post()
  @HttpCode(200)
  @ApiConsumes('application/x-www-form-urlencoded')
  async createAdmin(@Body() createAdminDto: CreateAdminDto) {
    return await this.adminsService.createAdmin(createAdminDto);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles('superAdmin')
  @ApiOperation({ summary: 'Edit existing admin.' })
  @Post('/edit/:id')
  @HttpCode(200)
  @ApiConsumes('application/x-www-form-urlencoded')
  async editAdmin(@Body() updateAdminDto: UpdateAdminDto, @Param() adminIdDto: AdminIdDto) {
    return await this.adminsService.updateAdmin(adminIdDto.id, updateAdminDto);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @ApiOperation({ summary: 'Update your own profile.' })
  @Post('update')
  @HttpCode(200)
  @ApiConsumes('application/x-www-form-urlencoded')
  async updateProfile(@Body() updateProfileDto: UpdateProfileDto, @GetUser() user) {
    return await this.adminsService.updateProfile(user._id, updateProfileDto);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @ApiOperation({ summary: 'Update your password.' })
  @Post('update-password')
  @HttpCode(200)
  @ApiConsumes('application/x-www-form-urlencoded')
  async updatePassword(@Body() updatePasswordDto: UpdatePasswordDto, @GetUser() user) {
    return await this.adminsService.updatePassword(user._id, updatePasswordDto);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles('superAdmin')
  @ApiOperation({ summary: 'Suspend admin.' })
  @Post('/suspend/:id')
  @HttpCode(200)
  @ApiConsumes('application/x-www-form-urlencoded')
  async suspendAdmin(@Param() adminIdDto: AdminIdDto) {
    return await this.adminsService.setSuspended(adminIdDto.id, true);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles('superAdmin')
  @ApiOperation({ summary: 'Unsuspend admin.' })
  @Post('/un-suspend/:id')
  @HttpCode(200)
  @ApiConsumes('application/x-www-form-urlencoded')
  async unSuspendAdmin(@Param() adminIdDto: AdminIdDto) {
    return await this.adminsService.setSuspended(adminIdDto.id, false);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles('superAdmin')
  @Get()
  async getAdmins(@Query() adminsListDto: AdminsListDto) {
    adminsListDto.keyword = adminsListDto.keyword ? adminsListDto.keyword : '';

    return await this.adminsService.getAdmins(adminsListDto);
  }
}
